
const Course = require("../models/course.js");



// module.exports.addCourse = (reqBody) => {

// 	let newCourse = new Course({
// 		name: reqBody.name,
// 		description: reqBody.description,
// 		price: reqBody.price
// 	})

// 	return newCourse.save().then((newCourse, error) => {
// 		if(error){
// 			return error;
// 		}
// 		else{
// 			return newCourse;
// 		}
// 	})
// };

// s39 ACTIVITY
// create a single course

module.exports.addCourse = (data) => {
	console.log(data.isAdmin)

	if(data.isAdmin) {
		let newCourse = new Course({
			name: data.course.name,
			description: data.course.description,
			price: data.course.price
		});

		return newCourse.save().then((newCourse, error) => {
			if(error){
				return error
			}
			return newCourse
		})
	};
	// If the user is not admin, then return this message as a promise to avoid errors
	let message = Promise.resolve('User must be ADMIN to access this')

	return message.then((value) => {
		return{value}
	})
};/*create a single course end*/

	

module.exports.getAllCourse = () => {
	return Course.find({}).then (result => {
		return result;
	})
};

module.exports.getActiveCourses = () => {
	return Course.find({}).then (result => {
		return result;
	})
};




// GET specficic course
module.exports.getCourse = (courseId) => {
						// inside the parenthesis should be the id
	return Course.findById(courseId).then(result => {
		return result;
	})
};
							// it will contain multiple fields
module.exports.updateCourse = (courseId, newData) => {
	if(newData.isAdmin == true) {
		return Course.findByIdAndUpdate(courseId, 
			{		/*req.body*/
				name: newData.course.name,
				description: newData.course.description,
				price: newData.course.price
			}
		).then((updatedCourse, error) => {
			if(error){
				return false
			}
			return true
		})

		}
		else{
			let message = Promise.resolve('User must be ADMIN to access this!')
			return message.then((value) => {return value})
	}
}

s40 activity
module.exports.archiveCourse = (courseId) => {
	return Course.findByIdAndUpdate(courseId, {
		isActive: false
	})

	.then((archivedCourse, error) => {
		if(error) {
			return false
		}

		return{
			message: "Course archived successfully"
		}
	})
};

// module.exports.archiveCourse = (courseId) => {
//     if(courseId.isAdmin == true){
//     return Course.findByIdAndUpdate(courseId, {
//         isActive: false
//     })
//     .then((archiveCourse, error) => {
//         if(error){
//             return false
//         }
//             return{
//                 message: "Course archived successfully!"
//             }
//     })
//     }
//     else{
//         let message = Promise.resolve('User must be ADMIN to access this');
//             return message.then((value) => {return value})
//     }
// };

// S41 Activity
module.exports.enroll = async (data) => {

	let isUserUpdated = await User.findById(data.userId).then(user => {

		user.enrollments.push({courseId: data.courseId});

		return user.save().then((user, error) => {
			if(error) {
				return false;
			} else{
				return true;
			}
		})

	})

	let isCourseUpdated = await Course.findById(data.courseId).then(course => {

		course.enrollees.push({userId : data.userId});

		return course.save().then((course, error) => {
			if(error) {
				return false;
			} else {
				return true;
			}
		})
	})

	if(isUserUpdated && isCourseUpdated) {
		return true;
	} else {
		return false;
	}

};
/*=============================================*/